import React,{useState} from 'react'
import sidebanner from '../Logo/sidebanner.png'
import play from '../Logo/play.png'
import logo from '../Logo/logo.png'
import burger from '../Logo/burger.png'
import s1 from '../Logo/s1.png'
import s2 from '../Logo/s2.png'
import s3 from '../Logo/s3.png'
import down from '../Logo/down.png'
import './HairDashboard.scss'
export default function HairDashboard() {
  const [show,setshow]=useState("")
  const [datainfo,setdatainfo]=useState("")
  const cards=[
    {
      img:s1,
      name:"Men",
      mobile:"My Specialty Is"
    },
    {
      img:s2,
      name:"Women",
      mobile:"My Specialty Is"
    },
    {
      img:s3,
      name:"Shop",
      mobile:"I Own The"
    },
  ]

  const datafuntion=(e)=>{
    setshow(e)
    setdatainfo(e)
  }
  return (
    <div className="dahboard-hair">

      <div className='h-navbar'>
<div className="first">
    <img className='burger' src={burger} alt="" />
    <img className='logo' src={logo} alt="" />
</div>
<div className="last">
<label className='f-label'>Get Started</label>
<label className='l-label'>Apps</label>
</div>
      </div>

<div className="dash-section" onMouseOver={()=>datafuntion("")}>
<div className='section-one'>
<h1>You’re Industry Is About To</h1>
<h2> Change Forever</h2>

<label htmlFor="">
<img src={play} alt="" />
Learn Why
</label>
</div>

<div className='downarrow'>
  <a href="#section1">Learn More</a>

  <img src={down} alt="" />
</div>
</div>



<div className='cards' id="section1">
{
  cards.map(item=>{
    return(
      <div className='cards-tite ' onMouseOver={()=>datafuntion(item.name)} >
        <div className='sub-card'>
        
        <p style={item.name==="Women"?{color:"black"}:{color:"white"}}>{item.mobile}</p>
        <h5>{item.name}</h5>
          </div>
        <div className="image-container">
        <img src={item.img} alt="" />
        </div>
      
   
 
       
       
      </div>
    )
  })
}
</div>
    </div>
  )
}
